<!DOCTYPE html>
<html>
<head>
    <title></title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <style type="text/css">

        .tithead{
            font-size: 40px;
            color: purple;
        }
        .title{
            font-size: 230%;
            line-height: 1.25em;
            padding-bottom: 26px;
        }

        .summary-list {
            height: 224px;
            overflow: auto;
            padding-bottom: 8px;
            margin-top: 20px;
            margin-bottom: 13px;
            border-style: none;
            border-bottom: solid 1px #d2d2d2;
        }


        .footerdiv{
            text-align: right;
            border-style: none;
            border-top: solid 1px #d2d2d2;
            padding-top: 20px;
        }
        table thead tr th label span{
            color: black;
        }
        table tbody tr td label span{
            color: black;
        }
    </style>

</head>
<body>
<div class="container">
    <div class="col s12 tithead"><center>Multi Files Download in Laravel Using <span style="color:orange" >Jquery .promise()</span> method</center></div>
    <div class="row" style="padding-top:80px">

        <div class="col s12 title">
            Choose the download you want
        </div>
        <div class="col s12" >
            <table>
                <thead>
                <tr>
                    <th>
                        <label>
                            <input type="checkbox" id="all_Chk"/>
                            <span>File Name</span>

                        </label>
                    </th>

                    <th>Size</th>
                </tr>
                </thead>

                <tbody>

                
                <tr>
                    <td>
                        <label>
                            <input type="checkbox" class="indi_chk" name="filedata[]" value="dummy_pdf.pdf"><span>dummy_pdf.pdf</span>
                        </label>
                    </td>
                    <td class="siz_file">1.4MB</td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <input type="checkbox" class="indi_chk" name="filedata[]" value="malayam.txt">
                            <span>malayam.txt</span>
                        </label>
                    </td>
                    <td class="siz_file">3.76MB</td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <input type="checkbox" class="indi_chk" name="filedata[]" value="SUPPLY.doc"/>
                            <span>SUPPLY.doc</span>
                        </label>
                    </td>
                    <td class="siz_file">7.00MB</td>
                </tr>
                </tbody>
            </table>
        </div>
       
    </div>

    <div class="row footerdiv">
        <button disabled class="btn waves-effect waves-light final_sub" name="action">Next

        </button>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

</body>
</html>

